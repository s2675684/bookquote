package nl.utwente.di.bookQuote ;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/**
 *Tests the Quoter
 */

public class TestQuoter {
    @Test
    public void testBook1 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("1") ;
        Assertions.assertEquals(10.0,price,0.0,"Priceofbook1");
    }
    @Test
    public void testBook2 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("2") ;
        Assertions.assertEquals(45.0,price,0.0,"Priceofbook2");
    }
    @Test
    public void testBook3 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("3") ;
        Assertions.assertEquals(20.0,price,0.0,"Priceofbook3");
    }
    @Test
    public void testBook4 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("4") ;
        Assertions.assertEquals(35.0,price,0.0,"Priceofbook4");
    }
    @Test
    public void testBook5 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("5") ;
        Assertions.assertEquals(50.0,price,0.0,"Priceofbook5");
    }
    @Test
    public void testBook6 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice("others") ;
        Assertions.assertEquals(0.0,price,0.0,"Priceofbook1");
    }

}